package Controller;

public interface Controllable {
   void buildingHouse();
   void bathRepairing();
   void bathMaterials();
   void sortMaterials();
}
