package Controller;

import model.Logic;

public class Controller implements Controllable {

    private Logic logic = new Logic();


    @Override
    public void buildingHouse() {
        logic.printInfoBathHouse();
    }

    @Override
    public void bathRepairing() {
        logic.printInfoRoomMaterials();
    }

    @Override
    public void bathMaterials() {
        logic.printInfoBathMaterials();
    }

    @Override
    public void sortMaterials() {
        logic.sortMaterialsParameters();
    }
}
