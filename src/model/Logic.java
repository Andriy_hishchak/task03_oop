package model;


import model.Material.BathMaterials;
import model.Material.HouseMaterials;
import model.Material.Material;
import model.Material.RoomMaterials;

import java.util.*;

public class Logic {
    private Map<String, Material> mapMaterials;


    public Logic() {
        mapMaterials = new LinkedHashMap<>();

        mapMaterials.put("1", new BathMaterials(210, "Bath"));
        mapMaterials.put("2", new BathMaterials(110, "Shaver"));
        mapMaterials.put("3", new BathMaterials(20, "Mirror"));

        mapMaterials.put("4", new RoomMaterials(2100, "Laminate"));
        mapMaterials.put("5", new RoomMaterials(1203, "Paint"));
        mapMaterials.put("6", new RoomMaterials(900, "Wallpaper"));

        mapMaterials.put("7", new HouseMaterials(300, "Concrete"));
        mapMaterials.put("8", new HouseMaterials(150, "Brick"));
        mapMaterials.put("9", new HouseMaterials(15, "Stone"));

    }
    public void sortMaterialsParameters() {
        System.out.println("Ви хочете знайти продукти які будуть подешевше?");
        System.out.println("Дешевше якої ціни?");
        System.out.println("Print: ");
        double Prise = GoodInt();
        StringBuilder res = new StringBuilder(" Materials :\n ");
        for (int i = 0; i < mapMaterials.size(); i++) {
            if (mapMaterials.get((i + 1) + "").getPrice() < Prise) {
                res.append(mapMaterials.get((i + 1) + "").getNameProduct()).append(" (");
                res.append(mapMaterials.get((i + 1) + "").getPrice()).append(")\n ");
            }
        }
        System.out.print(res);
    }

    public void printInfoBathMaterials() {
        LinkedList<Material> listBatMaterialsBath = new LinkedList<>();
        for (int i = 0; i < mapMaterials.size(); i++) {
            if (mapMaterials.get((i + 1) + "") instanceof BathMaterials) {
                //  System.out.print(mapMaterials.get(i + 1 + "").toString());
                listBatMaterialsBath.add(mapMaterials.get(i + 1 + ""));

            }
        }
        UserMaterials(listBatMaterialsBath);
    }

    public void printInfoBathHouse() {
        LinkedList<Material> listBatMaterials = new LinkedList<>();
        for (int i = 0; i < mapMaterials.size(); i++) {
            if (mapMaterials.get((i + 1) + "") instanceof HouseMaterials) {
                //  System.out.print(mapMaterials.get(i + 1 +"").toString());
                listBatMaterials.add(mapMaterials.get(i + 1 + ""));
            }
        }
        UserMaterials(listBatMaterials);
    }

    public void printInfoRoomMaterials() {
        LinkedList<Material> listBatMaterialsRoom = new LinkedList<>();

        for (int i = 0; i < mapMaterials.size(); i++) {
            if (mapMaterials.get((i + 1) + "") instanceof RoomMaterials) {
                //System.out.print(mapMaterials.get(i + 1 +"").toString());
                listBatMaterialsRoom.add(mapMaterials.get(i + 1 + ""));
            }
        }
        UserMaterials(listBatMaterialsRoom);
    }

    private void UserMaterials(LinkedList<Material> list) {
        ArrayList<Material> UserList = new ArrayList<>();
        boolean log_exit;
        double suma = 0;
        int productCaont = 0;
        StringBuilder proguktu = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            System.out.print("[" + i + "]" + list.get(i));
        }
        System.out.print("Вам потрібен якись товар із цієї підбірки?\n[1]Так\n[2]Ні\n");
        int vubirPdbirku = GoodInt();
        if (vubirPdbirku == 1) {
            do {
                System.out.print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
                for (int i = 0; i < list.size(); i++) {
                    System.out.print("[" + i + "]" + list.get(i));
                }


                System.out.print("Який товар вам потрібен?\nPrint: ");
                int vubir = GoodInt();
                System.out.print("Produkt : " + list.get(vubir).getNameProduct() + " | Prise  - (");
                UserList.add((list.get(vubir)));

                System.out.println(UserList.get(productCaont).getPrice() + ")");
                System.out.print("У якій кількості вам потрібен цей товар?\nPrint: ");
                int count = GoodInt();
                proguktu.append(UserList.get(productCaont).getNameProduct()).append(" [").append(count).append("]").append("\n");
                suma += UserList.get(productCaont).getPrice() * count;

                System.out.println("\n\nЧек :\n" +
                        "Товар: " + list.get(productCaont).getNameProduct() + "\n" +
                        "Кількість товару: " + count);
                System.out.print("___________________\n" +
                        "|Покупка уже коштує :" + suma + "\n"
                        + "-------------------\n");

                System.out.print("Це всі покупки у даній підбірці?\n[1]Так\n[2]Ні\n");
                int vubor = GoodInt();
                if (vubor == 1) {
                    log_exit = false;
                } else {
                    productCaont++;
                    log_exit = true;
                }

            } while (log_exit);
            Loading();
            System.out.print("Дрокуємо чек, зачекайте будьласка!\n");
            System.out.print("******************\n"
                    + "Ви узили такі продукти як :\n " + proguktu + "\n"
                    + "До сплата: [ " + suma + " ]\n");

        }

    }
      private void waitTime(double seconds){
        try{
            Thread.sleep((long) (seconds*1000));
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
     private int GoodInt() {

        Scanner in = new Scanner(System.in);
        int number;
        do {

            while (!in.hasNextInt()) {
                System.out.println("That not a number!");
                in.next(); // this is important!
            }
            number = in.nextInt();
        } while (number < 0);
        System.out.println("Thank you! Got menu " + number);
        return number;
    }
    private void Loading(){
        int sumwol = 9608;
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("                           _________________    ");
        System.out.print(  "                  Loading |");
        for(int i = 0; i < 17;i++) {
            Random random = new Random();
            boolean boll = random.nextBoolean();
            if(boll){
                waitTime(0.2);
                System.out.print((char) sumwol);
            }else{
                waitTime(0.3);
                System.out.print((char) sumwol);
            }


        }
        System.out.print("| \n");
    }
}
