package model.Material;

public abstract class Material {

    private double price;
    private String nameProduct;


    Material(double price, String nameProduct) {

        this.price = price;
        this.nameProduct = nameProduct;

    }

    public double getPrice() {
        return price;
    }

    public String getNameProduct() {
        return nameProduct;
    }
    @Override
    public String toString() {
        return     "_______________________________________\n"
                + "| Prodoct - [ " + nameProduct +" ]\n" +
                  "| Price - [ "+ price + " ] \n"
                + "|_________________________________________|\n";
    }
}
