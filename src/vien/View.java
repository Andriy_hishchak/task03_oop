package vien;

import Controller.Controller;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;

public class View {
    private Map<String, Control> controlMap;
    private Map<String, Control> controlMapNext;
    private LinkedList<String> menu;
    private LinkedList<String> menuNext;
    private Controller controller;

    public View() {
        controller = new Controller();

        controlMap = new LinkedHashMap<>();
        controlMap.put("1", this::findMaterials);
        controlMap.put("2", this::sortMaterials);

        controlMapNext = new LinkedHashMap<>();
        controlMapNext.put("1", this::buildingHouse);
        controlMapNext.put("2", this::bathMaterials);
        controlMapNext.put("3", this::roomRepairing);

        menuNext = new LinkedList<>();
        menuNext.add("________________________________________________");
        menuNext.add("| 1 - Building house                            |");
        menuNext.add("| 2 - Bath repairing                            |");
        menuNext.add("| 3 - Room repairing                            |");
        menuNext.add("| Back                                          |");
        menuNext.add("|_______________________________________________|");


        menu = new LinkedList<>();
        menu.add("________________________________________________");
        menu.add("|                                               |");
        menu.add("| 1 - Which materials do you want for update    |");
        menu.add("| 2 - Search for material by price              |");
        menu.add("| Exit                                          |");
        menu.add("|_______________________________________________|");
    }

    private void findMaterials() {
        String answ;
        Scanner scanner = new Scanner(System.in);

        do {
            printInfo(menuNext);
            answ = scanner.nextLine().toUpperCase();
            try {
                controlMapNext.get(answ).print();
            } catch (Exception ignore){
                if(!answ.equals("BACK")) {
                    System.out.println("Something went wrong");
                }
            }
        } while (!answ.equals("BACK"));
    }
     private void sortMaterials() {
      controller.sortMaterials();
    }
    private void buildingHouse() {
        controller.buildingHouse();
    }
    private void bathMaterials() {
        controller.bathMaterials();
    }
    private void roomRepairing() {
        controller.bathRepairing();
    }

    public void start() {
        String answ;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome");
        System.out.print("Ви знаходетесь у нашому Гіпермаркеті будівельних матеріалів!\n");
        System.out.print("І таму скоріше за все у вас іде ремонт дому!\n");
        System.out.print("Виберіть якого характеру у вас іде ремонт, щоб оприділитись із матеріалами які вам потрібні!");
        System.out.print("Ми допоможиво вам оприділитись із вибором!\n");
        do {
          printInfo(menu);
          answ = scanner.nextLine().toUpperCase();
          try {
                controlMap.get(answ).print();
            } catch (Exception ignore){
              if(!answ.equals("BACK")) {
                  System.out.println("Something went wrong");
              }
          }
        } while (!answ.equals("EXIT"));
    }
    private void printInfo(LinkedList<String> printMenu) {
        for (String str : printMenu) {
            System.out.println(str);
        }
    }
}


